using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LSystemGenerator : MonoBehaviour
{
    [Range(1,5)]
    [SerializeField] int generations = 1;
    [SerializeField] GameObject geoPrefab;
    [SerializeField] Transform result;
    [SerializeField] float angle = 25f;
    [SerializeField] float size = 10f;
    string axiom ="F";
    string sentence;
    Dictionary<char, string> rules = new Dictionary<char, string>();
    Stack<TransformInfo> transformStack = new Stack<TransformInfo>();
    WaitForSeconds wfs = new WaitForSeconds(0.05f);

    void Start()
    {
        sentence = axiom;
        rules.Add('F', "FF+[+F-F-F]-[-F+F+F]");
        for (int i = 0; i < generations; i++)
        {
            Generate();
            //size *= 0.6f;
            //angle *= 0.5f;
        }
        StartCoroutine(GenerateCO());
    }

    private void Generate()
    {
        StringBuilder sb = new StringBuilder("");
        for (int i = 0; i < sentence.Length; i++)
        {
            char curChar = sentence[i];
            if (rules.ContainsKey(curChar))
            {
                sb.Append(rules[curChar]);
            }
            else
            {
                sb.Append(curChar);
            }
        }

        sentence = sb.ToString();
        //Debug.Log(sentence);
    }

    IEnumerator GenerateCO()
    {
        for (int i = 0; i < sentence.Length; i++)
        {
            Vector3 randDir = new Vector3(Random.Range(1f, 6f), Random.Range(0f, 3f), Random.Range(0f, 3f));
            char curChar = sentence[i];
            switch (curChar)
            {
                case 'F':
                    Vector3 initPos = transform.position;
                    transform.Translate(Vector3.up * size);
                    var geo = Instantiate<GameObject>(geoPrefab, initPos, transform.rotation, result);
                    geo.transform.localScale = new Vector3(size, size, size);
                    break;
                case '+':
                    transform.Rotate(randDir * angle);
                    break;
                case '-':
                    transform.Rotate(randDir * -angle);
                    break;
                case '[':
                    TransformInfo ti = new TransformInfo(transform.position, transform.rotation);
                    transformStack.Push(ti);
                    break;
                case ']':
                    TransformInfo ti2 = transformStack.Pop();
                    transform.position = ti2.Position;
                    transform.rotation = ti2.Rotation;
                    break;
                default:
                    break;
            }
            //yield return wfs;
            yield return null;
        }
    }
}

public struct TransformInfo
{
    public Vector3 Position { get; private set; }
    public Quaternion Rotation { get; private set; }

    public TransformInfo (Vector3 position, Quaternion rotation)
    {
        Position = position;
        Rotation = rotation;
    }
}
